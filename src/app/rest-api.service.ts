import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  user: string;
  pwd: string;
  dataTest: Observable<any>;
  loadingShown = false;
  messageLogin: string;

  constructor(public http: HttpClient, public loadingCtrl: LoadingController) {

  }

  public async postData(user, pwd) {
    let loading = await this.loadingCtrl.create({
      spinner: 'circles',
      message: 'please wait...'
    });
    loading.present().then(() => {
      this.loadingShown = true;
      return new Promise((resolve, reject) => {
        let body = {
          'USER_NAME': user,
          'USER_PWD': pwd
        }

        this.http.post('http://122.155.1.30:51000/api/Customer/CheckLogin', body, {
          headers: { 'Content-Type': 'application/json' }
        }).subscribe(data => {
          loading.dismiss().then(() => {
            this.loadingShown = false;
            resolve(data)
            console.log('Response: ', data);
          });
        }, error => {
          loading.dismiss().then(() => {
            this.loadingShown = false;
            reject(error);
            console.log('Error: ' + error)
          })
        }
        )
      })
    })
  }


}

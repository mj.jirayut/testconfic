import { Component } from '@angular/core';
import { RestApiService } from '../rest-api.service';
import { NavController } from '@ionic/angular';
import { threadId } from 'worker_threads';
import { RegisterPage } from '../register/register.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public service: RestApiService, public navCtrl : NavController, ) {

  }
  login(username, password) {
  
    if (username == "" || password == "") {
      alert("กรุณากรอกข้อมูล!")
    } else {
      this.service.postData(username, password)
     
      
    }

  }

  register() {
    
  }
 

}
